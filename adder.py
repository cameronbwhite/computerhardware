def halfAdder(a, b):
    return (a ^ b, a & b)

def fullAdder(a, b, cin):
    return (a ^ b ^ cin, (a & b) ^ (a & cin) ^ (b & cin))

def fullAdder2(a, b, cin):
    (s0, c0) = halfAdder(a, b)
    (s, c1) = halfAdder(s0, cin)
    return (s, c0 ^ c1)
