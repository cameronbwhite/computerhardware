import Data.Bits 

halfAdder :: Bits a => a -> a -> (a, a)
halfAdder x y = (x `xor` y, x .&. y)

fullAdder :: Bits a => a -> a -> a -> (a, a)
fullAdder x y c_in = (s, c_out)
    where s = (x `xor` y) `xor` c_in;
          c_out = ((x .&. y) .|. (x .&. c_in)) .|. (y .&. c_in)

fullAdder2 :: Bits a => a -> a -> a -> (a, a)
fullAdder2 x y c_in = (s, c_out)
    where (s0, c0) = halfAdder x y;
          (s, c1) = halfAdder s0 c_in;
          c_out = c0 `xor` c1
