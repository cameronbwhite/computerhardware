and(1, 1, 1).
and(_, _, 0).

or(0, 0, 0).
or(_, _, 1).

xor(1, 1, 0).
xor(0, 0, 0).
xor(_, _, 1).

andMany([B|[]], B).
andMany([B|Bs], X) :-
    andMany(Bs, X0),
    and(B, X0, X).

orMany([B|[]], B).
orMany([B|Bs], X) :-
    orMany(Bs, X0),
    or(B, X0, X).

halfAdder(A, B, S, C) :-
    xor(A, B, S),
    and(A, B, C).

fullAdder(A, B, Cin, S, Cout) :-
    xor(A, B, S0),
    xor(S0, Cin, S),
    and(A, B, C0),
    and(S0, Cin, C1),
    or(C0, C1, Cout).

fullAdder2(A, B, Cin, S, Cout) :-
    halfAdder(A, B, S0, C0),
    halfAdder(S0, Cin, S, C1),
    xor(C0, C1, Cout).

fastAdderCell(A, B, C, G, P, S) :-
    and(A, B, G),
    xor(A, B, P),
    xor(P, C, S).

fastAdder4([A0, A1, A2, A3], [B0, B1, B2, B3],
           C0, [S0, S1, S2, S3], C4, G, P) :-
    fastAdderCell(A3, B3, C0, G0, P0, S3),
    fastAdderCell(A2, B2, C1, G1, P1, S2),
    fastAdderCell(A1, B1, C2, G2, P2, S1),
    fastAdderCell(A0, B0, C3, G3, P3, S0),
    orMany([G0, Z0], C1), 
        and(P0, C0, Z0),
    orMany([G1, Z2, Z1], C2), 
        and(P1, G0, Z2), and(P1, Z0, Z1),
    orMany([G2, Z5, Z4, Z3], C3), 
        and(P2, G1, Z5), and(P2, Z2, Z4), and(P2, Z1, Z3),
    orMany([G, Z6], C4),
        and(P3, G2, Z9), and(P3, Z5, Z8), and(P3, Z4, Z7), and(P3, Z3, Z6),
    andMany([P0, P1, P2, P3], P),
    orMany([G3, Z9, Z8, Z7], G).
